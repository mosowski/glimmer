uniform vec3 eyePosition;
uniform vec3 eyeDirection;
uniform vec3 eyePlane[4];
uniform vec3 times;
uniform sampler2D noiseTexture;

#define marchIterations 150.0
#define epsilon 0.003
#define far 500.0
#define reflections 2.0
#define softShadowFactor 16.0
#define specularPower 64.0

#define material0 1024.0
#define material1 1.0
#define material2 2.0
#define material3 4.0
#define material4 8.0

#define pie 3.14159265

vec2 rotate(vec2 o, float a) {
    float c = cos(a);
	float s = sin(a);
    return vec2(o.x * c - o.y * s, o.x * s + o.y * c);
}

vec3 translate(vec3 o, vec3 t) {
	return o - t;
}

float sphere(vec3 o, float radius) {
	return length(o) - radius;
}

float plane(vec3 o, vec3 normal, float offset) {
	return dot(o, normal) - offset;
}

float torus(vec3 o, float ri, float ro) {
    vec2 q = vec2(length(o.xz) - ri, o.y);
    return length(q) - ro;
}

float column(vec3 o, float r) {
    return length(o.xz) - r;
}

float box(vec3 o, vec3 size) {
	vec3 dist = abs(o) - size;
	return min(max(dist.x, max(dist.y, dist.z)),0.0) + length(max(dist, 0.0));
}

float replicate(float o, float cell) {
	return mod(o, cell) -  cell * 0.5;
}

void sum(inout vec2 a, in vec2 b) {
	if (b.x < a.x) {
		a = b;
	}
}

void diff(inout vec2 a, in vec2 b) {
	if (-a.x < b.x) {
		a = b;
	} else {
		a.x = -a.x;
	}
}

void common(inout vec2 a, in vec2 b) {
	if (a.x < b.x) {
		a = b;
	}
}

float fancy_figure(vec3 o) {
	o.xz = rotate(o.xz, o.y * 0.5 * abs(times.y - 0.5));
	float d = sphere(o, 2.5);
	d = min(d, box(o, vec3(1.0,2.5,2.0)));
	d = min(d, box(o, vec3(3.5, 0.5, 0.5)));
	d = min(d, torus(o, 3.5, 0.5));
	d = min(d, torus(o.yxz, 3.5, 0.5));
	float distortion = sin(10.0*o.x) * sin(10.0*o.y) * sin(10.0*o.z) * 0.1 * abs(times.y - 0.5);
	return d + distortion;
}

vec2 fancy_figures(vec3 o) {
	float cellSize = 20.0;
	float cellFactor = abs(sin(floor(o.z / cellSize) + floor(o.x / cellSize)));
	o.x = replicate(o.x, cellSize);
	/*figo.y = replicate(figo.y, cellSize);*/
	o.z = replicate(o.z, cellSize);
	float ang = times.x + cellFactor * 3.0;
	o.xz = rotate(o.xz, ang);
	return vec2(fancy_figure(o), material2);
}


vec2 spheres(vec3 o) {
	float cellSize = 20.0;
	float cellFactor = abs(sin(floor(o.z / cellSize) + floor(o.x / cellSize)));
	o.x = replicate(o.x, cellSize);
	o.z = replicate(o.z, cellSize);
	float d = sphere(o, 4.0);
	d = max(-d, box(o - vec3(2.0, 0.0, 0.0), vec3(4.5, 2.0, 2.0)));
	return vec2(d, material3);
}

vec2 distance(vec3 o) {
	vec2 d = fancy_figures(o);
	sum(d, spheres(o - vec3(10.0, 0.0, 10.0)));

	sum(d, vec2(plane(o, vec3(0.0, 1.0, 0.0), -4.0), material1));
	return d;
}

vec3 normal(vec3 target) {
	vec2 eps = vec2(0.0001, 0.0);
	return normalize(vec3(distance(target + eps.xyy).x - distance(target - eps.xyy).x, distance(target + eps.yxy).x - distance(target - eps.yxy).x , distance(target + eps.yyx).x - distance(target - eps.yyx).x));
}

vec3 pack_normal(vec3 n) {
	return n * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
}

vec2 march(vec3 pos, vec3 dir) {
	float travel = epsilon * 2.0;
	for (float i = 0.0; travel < far && i < marchIterations; i++) {
		vec2 hit = distance(pos + dir * travel);
		float d = length(hit.x);
		if (d < epsilon || travel > far) {
			return vec2(travel, hit.y);
			break;
		}
		travel += d * 0.5;
	}
	return vec2(travel, material0);
}

float ambient_occlusion(vec3 pos, vec3 n) {
	float stepSize = 0.08;
	float t = stepSize;
	float oc = 0.0;
	for(float i = 0.0; i < 5.0; ++i)
	{
		float d = distance(pos + n * t).x;
		oc += t - d;
		t += stepSize;
		stepSize *= 1.2;
	}
	return 1.0 - clamp(oc, 0.0, 0.8);
}

float shadow(vec3 pos, vec3 dir, float lightDist) {
	float travel = epsilon * 2.0;
	float factor = 1.0;
	for (float i = 0.0; travel < lightDist && i < marchIterations; i++) {
		float hit = distance(pos + dir * travel).x;
		float d = length(hit);
		if (d < epsilon) {
			factor = 0.0;
			break;
		}
		factor = min(factor, softShadowFactor * d / travel);
		travel += d * 0.5;
	}
	return factor;
}

vec3 phong(vec3 pos, vec3 n, vec3 rayDir, vec3 refRayDir, vec3 lightPos, vec3 lightColor) {
	vec3 lightDir = normalize(lightPos - pos);
	float intensity = dot(n, lightDir);
	intensity *= shadow(pos, lightDir, length(lightPos - pos));
	intensity = max(0.3, intensity);

	vec3 color = intensity * lightColor;
	float spec = pow(clamp(dot(refRayDir, lightDir), 0.0, 1.0), specularPower);
	color += lightColor * spec * intensity;
	return color;
}

vec3 material_color(in vec2 hit, in vec3 hitPos) {
	if (hit.y < material2) { 
		if (mod(floor(hitPos.x) + floor(hitPos.z), 2.0) < 1.0) {
			return vec3(1.0, 1.0, 1.0);
		} else {
			return vec3(0.9, 0.9, 0.9);
		}
	} else if (hit.y < material3) { 
		return vec3(0.3, 1.0, 0.5);
	} else if (hit.y < material4) { 
		return vec3(1.0, 0.3, 0.3);
	}
	return vec3(0.8, 0.8, 0.8);
}

vec3 lighting(vec3 rayPos, vec3 rayDir) {
	vec3 lightPos = vec3(10.0 * sin(times.x*0.3), 10.0, 10.0 * cos(times.x*0.1));
	vec3 totalColor = vec3(0.0, 0.0, 0.0);
	float reflectionPower = 1.0;
	for (float i = 0.0; i < reflections; i++) {
		vec2 hit = march(rayPos, rayDir);
		float hitDist = hit.x;
		if (hitDist > far) {
			break;
		}
		vec3 hitPos = rayPos + rayDir * hitDist;
		vec3 n = normal(hitPos);
		vec3 refRayDir = reflect(rayDir, n);

		vec3 color = phong(hitPos, n, rayDir, refRayDir, lightPos, vec3(1.0, 0.7, 0.5));
		/*color += phong(hitPos, n, rayDir, refRayDir, lightPos.zyx, vec3(0.1, 0.3, 0.6)*1.5);*/
		color *= ambient_occlusion(hitPos, n);
		color *= material_color(hit, hitPos);

		totalColor += color * reflectionPower;

		reflectionPower *= 0.33;

		rayPos = hitPos;
		rayDir = refRayDir;
	}
	return totalColor;
}

in vec2 var_texCoord0;
out vec4 out_fragColor;

void main() {
	vec2 uv = var_texCoord0.xy;

	vec3 top = mix(eyePlane[0], eyePlane[1], uv.x);
	vec3 bottom = mix(eyePlane[2], eyePlane[3], uv.x);
	vec3 rayDir = normalize(mix(bottom, top, uv.y));

	vec3 color = lighting(eyePosition, rayDir);
	out_fragColor = vec4(color.x, color.y, color.z, 1);
}
