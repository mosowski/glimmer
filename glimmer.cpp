// 
// Build:
// clang++ -framework GLUT -framework OpenGL -framework Cocoa -std=c++11 -Wno-deprecated glimmer.cpp thirdparty/lodepng/lodepng.cpp -o glimmer
//

#ifdef _WIN32
#define _USE_MATH_DEFINES
#include <glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glext.h>
#else
#include <glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#endif

#include <array>
#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <streambuf>
#include <map>
#include <cmath>

#include "thirdparty/lodepng/lodepng.h"

int windowWidth = 800;
int windowHeight = 600;
char *shaderFileName;

float alphaAngle = 0;
float betaAngle = 0;
float planeDistance = 1;

bool keys[256];
int xMouseLast;
int yMouseLast;

std::vector<std::pair<unsigned int, unsigned int>> textures;

int shaderProgram;

// uniforms
int eyePositionUniform;
std::array<float, 3> eyePosition = { 0, 0, 0 };

int eyePlaneUniform;
std::array<std::array<float, 3>, 4> eyePlane;

int eyeDirectionUniform;
std::array<float, 3> eyeDirection = { 0, 0, 1 };

int timesUniform;
std::array<float, 3> times = { 0, 0, 0 };

// math
void add_vec3_vec3(float *v, float *out) {
	out[0] += v[0];
	out[1] += v[1];
	out[2] += v[2];
}

void mult_float_vec3(float f, float *out) {
	out[0] *= f;
	out[1] *= f;
	out[2] *= f;
}

void mult_m44_vec3(float* m, float *out) {
	float tv[4] = { out[0], out[1], out[2], out[3] };
	out[0] = m[0] * tv[0] + m[1] * tv[1] + m[2] * tv[2];
	out[1] = m[4] * tv[0] + m[5] * tv[1] + m[6] * tv[2];
	out[2] = m[8] * tv[0] + m[9] * tv[1] + m[10] * tv[2];
}


std::string read_file(const char *path) {
	std::ifstream is(path);
	return std::string((std::istreambuf_iterator<char>(is)),
		std::istreambuf_iterator<char>());
}

void init() {
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glEnable(GL_DEPTH_TEST);
    glShadeModel(GL_SMOOTH);
}

void keyboard(unsigned char key, int x, int y) {
	keys[key] = true;
}

void keyup(unsigned char key, int x, int y) {
	keys[key] = false;
}

void motion(int x, int y) {
	alphaAngle -= ((float)x - xMouseLast) * 0.3f;
	betaAngle -= ((float)y - yMouseLast) * 0.3f;
	xMouseLast = x;
	yMouseLast = y;
}

void passivemotion(int x, int y) {
	xMouseLast = x;
	yMouseLast = y;
}

void process_input() {
	if (keys['w']) {
		mult_float_vec3(0.3f, eyeDirection.data());
		add_vec3_vec3(eyeDirection.data(), eyePosition.data());
	}
	if (keys['s']) {
		mult_float_vec3(-0.3f, eyeDirection.data());
		add_vec3_vec3(eyeDirection.data(), eyePosition.data());
	}
	if (keys['a']) {
		eyePosition[0] -= eyeDirection[2] * 0.3f;
		eyePosition[2] -= -eyeDirection[0] * 0.3f;
	}
	if (keys['d']) {
		eyePosition[0] += eyeDirection[2] * 0.3f;
		eyePosition[2] += -eyeDirection[0] * 0.3f;
	}
	if (keys['e']) {
		betaAngle += 1;
	}
	if (keys['q']) {
		betaAngle -= 1;
	}
}

void setup_eye_plane() {
	float aspect = (float)windowWidth / windowHeight;
	eyePlane[0] = { -aspect, 1, planeDistance };
	eyePlane[1] = { aspect, 1, planeDistance };
	eyePlane[2] = { -aspect, -1, planeDistance };
	eyePlane[3] = { aspect, -1, planeDistance };

	eyeDirection = { 0, 0, 1 };

	float eyeMatrix[16];
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glGetFloatv(GL_MODELVIEW_MATRIX, eyeMatrix);

	glRotatef(betaAngle, 1, 0, 0);
	glRotatef(alphaAngle, 0, 1, 0);
	glGetFloatv(GL_MODELVIEW_MATRIX, eyeMatrix);


	mult_m44_vec3(eyeMatrix, eyePlane[0].data());
	mult_m44_vec3(eyeMatrix, eyePlane[1].data());
	mult_m44_vec3(eyeMatrix, eyePlane[2].data());
	mult_m44_vec3(eyeMatrix, eyePlane[3].data());
	mult_m44_vec3(eyeMatrix, eyeDirection.data());
}

void setup_times() {
	times[0] = glutGet(GLUT_ELAPSED_TIME) / 1000.0f;
	times[1] = fmod(times[0], 1.0);
	times[2] = times[1] * 2.0 * M_PI;
}

void display() {
	process_input();
	setup_eye_plane();
	setup_times();

	glUseProgram(shaderProgram);

	glUniform3fv(eyePositionUniform, 1, eyePosition.data());
	
	glUniform3fv(eyePlaneUniform, 4, eyePlane[0].data());
	glUniform3fv(eyeDirectionUniform, 1, eyeDirection.data());
	glUniform3fv(timesUniform, 1, times.data());

	for (int i = 0; i < textures.size(); ++i) {
		glActiveTexture(GL_TEXTURE0 + i);
		glBindTexture(GL_TEXTURE_2D, textures[i].first);
		glUniform1i(textures[i].second, i);
	}

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    glBegin(GL_TRIANGLES);
        glTexCoord2f(0, 1);
        glVertex3f(0, 0, 0);
        glTexCoord2f(1, 1);
        glVertex3f(windowWidth, 0, 0);
        glTexCoord2f(0, 0);
        glVertex3f(0, windowHeight, 0);

        glTexCoord2f(0, 0);
        glVertex3f(0.0, windowHeight, 0);
        glTexCoord2f(1, 0);
        glVertex3f(windowWidth, windowHeight, 0);
        glTexCoord2f(1, 1);
        glVertex3f(windowWidth, 0, 0);
    glEnd();

    glutSwapBuffers();
}

void reshape(int w, int h) {
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
	glOrtho(0, windowWidth, windowHeight, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}

int load_texture(const char* path, int wrapMode, int filtering) {
	std::vector<unsigned char> imageData;
	unsigned int width;
	unsigned int height;

	unsigned int error = lodepng::decode(imageData, width, height, path);
	if (error != 0) {
		printf("Texture error (%s):\n%s\n", path, lodepng_error_text(error));
		exit(1);
	}

	unsigned int texture;

	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filtering);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filtering);

	glTexImage2D(GL_TEXTURE_2D, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &imageData[0]);

	return texture;
}

void read_settings(std::string& src) {
	std::vector<std::string> lines;

	bool begin = false;
	std::istringstream linesStream(src);
	std::string line;
	while (std::getline(linesStream, line)) {
		if (line.find("//###begin", 0) != std::string::npos) {
			begin = true;
		} else if (line.find("//###end", 0) != std::string::npos) {
			break;
		} else if (begin) {
			std::istringstream lineStream(line);
			std::string comment;
			lineStream >> comment;
			if (comment != "//") {
				printf("Settings error: Missing // at the line begin.\n");
			}
			std::string param;
			lineStream >> param;
			if (param == "texture") {
				std::string samplerName, fileName, wrapMode, filterMode;
				lineStream >> samplerName >> fileName >> wrapMode >> filterMode;
				unsigned int uniform = glGetUniformLocation(shaderProgram, samplerName.c_str());
				int wrapModeCode = wrapMode == "repeat" ? GL_REPEAT : GL_CLAMP;
				int filterModeCode = filterMode == "nearest" ? GL_NEAREST : GL_LINEAR;
				unsigned int texture = load_texture(fileName.c_str(), wrapModeCode, filterModeCode);

				std::cout << "Loaded texture " << fileName.c_str() << " as sampler " << samplerName << std::endl;

				textures.push_back(std::make_pair(texture, uniform));
			}
		}
	}
}

void setup_shaders() {
	int v = glCreateShader(GL_VERTEX_SHADER);
	int f = glCreateShader(GL_FRAGMENT_SHADER);	

	std::string vSrc = read_file("glimmer.vert");
	std::string fSrc = read_file(shaderFileName);

	const char *vSrcPtr = vSrc.c_str();
	const char *fSrcPtr = fSrc.c_str();
	glShaderSource(v, 1, &vSrcPtr, NULL);
	glShaderSource(f, 1, &fSrcPtr, NULL);

	glCompileShader(v);
	glCompileShader(f);

	int fCompiled;
	glGetShaderiv(f, GL_COMPILE_STATUS, &fCompiled);
	if(!fCompiled) {
		int logLength = 0;
		glGetShaderiv(f, GL_INFO_LOG_LENGTH, &logLength);
 
		std::vector<char> errorLog(logLength);
		glGetShaderInfoLog(f, logLength, &logLength, &errorLog[0]);

		printf("Shader Error:\n%s\n", &errorLog[0]);
		exit(1);
	}

	shaderProgram = glCreateProgram();
	
	glAttachShader(shaderProgram, v);
	glAttachShader(shaderProgram, f);

	glLinkProgram(shaderProgram);
	glUseProgram(shaderProgram);

	eyePositionUniform = glGetUniformLocation(shaderProgram, "eyePosition");
	eyePlaneUniform = glGetUniformLocation(shaderProgram, "eyePlane");
	eyeDirectionUniform = glGetUniformLocation(shaderProgram, "eyeDirection");
	timesUniform = glGetUniformLocation(shaderProgram, "times");

	std::cout << "Vars: " << eyePositionUniform << ", " << eyePlaneUniform << ", " << eyeDirectionUniform << std::endl;

	read_settings(fSrc);
}

int main(int argc, char **argv) {
	if (argc < 4) {
		printf("glimmer <width> <height> <fragment>\n");
		exit(1);
	}

	sscanf(argv[1], "%d", &windowWidth);
	sscanf(argv[2], "%d", &windowHeight);
	shaderFileName = argv[3];
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
    glutInitWindowSize(windowWidth, windowHeight);

    //glutInitWindowPosition(0, 0);
	//glutInitContextVersion(4,1);
	//glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
	//glutInitContextProfile(GLUT_CORE_PROFILE);

    glutCreateWindow("Glimmer");
    glutReshapeFunc(reshape);
    glutDisplayFunc(display);
    glutIdleFunc(display);
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyup);
	glutMotionFunc(motion);
	glutPassiveMotionFunc(passivemotion);

	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	printf("OpenGL %d.%d\n", OpenGLVersion[0], OpenGLVersion[1]);

	setup_shaders();

    init();

    glutMainLoop();
    return 0;
}
