uniform vec3 eyePosition;
uniform vec3 eyeDirection;
uniform vec3 eyePlane[4];
uniform vec3 times;

#define marchIterations 150.0
#define epsilon 0.0003
#define far 500.0
#define reflections 1.0
#define softShadowFactor 16.0
#define specularPower 64.0
#define bottomSkyColor vec3(1.0, 0.75, 0.6)
#define topSkyColor vec3(0.2, 0.66, 0.75)

#define material0 1024.0
#define material1 1.0
#define material2 2.0
#define material3 4.0
#define material4 8.0

#define pie 3.14159265

vec2 rotate(vec2 o, float a) {
    float c = cos(a);
	float s = sin(a);
    return vec2(o.x * c - o.y * s, o.x * s + o.y * c);
}

vec3 translate(vec3 o, vec3 t) {
	return o - t;
}

float sphere(vec3 o, float radius) {
	return length(o) - radius;
}

float plane(vec3 o, vec3 normal, float offset) {
	return dot(o, normal) - offset;
}

float torus(vec3 o, float ri, float ro) {
    vec2 q = vec2(length(o.xz) - ri, o.y);
    return length(q) - ro;
}

float column(vec3 o, float r) {
    return length(o.xz) - r;
}

float box(vec3 o, vec3 size) {
	vec3 dist = abs(o) - size;
	return min(max(dist.x, max(dist.y, dist.z)),0.0) + length(max(dist, 0.0));
}

float replicate(float o, float cell) {
	return mod(o, cell) -  cell * 0.5;
}

void sum(inout vec2 a, in vec2 b) {
	if (b.x < a.x) {
		a = b;
	}
}

void diff(inout vec2 a, in vec2 b) {
	if (-a.x < b.x) {
		a = b;
	} else {
		a.x = -a.x;
	}
}

void intersection(inout vec2 a, in vec2 b) {
	if (a.x < b.x) {
		a = b;
	}
}

float rand(vec2 co){
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float sharpen(float f) {
	return abs(cos(abs(f)));
}

float noise(vec2 o) {
	vec2 d = vec2(1.0, 0.0);
	vec2 f = floor(o * d.x);
	float a = mix(rand(f), rand(f + d.xy), smoothstep(0.0, 1.0, (o.x - f.x) / d.x));
	float b = mix(rand(f + d.yx), rand(f + d.xx), smoothstep(0.0, 1.0, (o.x - f.x) / d.x));
	return mix(a, b, smoothstep(0.0, 1.0, (o.y - f.y) / d.x));
}

float frac_noise(vec2 o) {
	vec3 ts = vec3(times.x, times.x * 0.75, times.x * 0.5);
	return 0.0
		+ noise(o * 0.125 + ts.xy) * 4.0 
		+ noise(o * 0.25 + ts.yx) * 2.0 
		+ noise(o * 0.5 + ts.yz) * 1.0 
		+ noise(o + ts.xx) * 0.5
		+ noise(o * 3.0 + ts.xz) * 0.05 
		;
}

float frac_noise_low(vec2 o) {
	vec3 ts = vec3(times.x, times.x * 0.75, times.x * 0.5);
	return 0.0
		+ noise(o * 0.06 + ts.xx * 0.2) * 1.3
		+ noise(o * 0.08 + ts.xy * 0.3) * 0.7
	;
}

float wave(vec2 o) {
	return pow(clamp(sharpen(frac_noise(o)), 0.0, 0.99), 0.75) + abs((frac_noise_low(o))) * 2.5;
}

vec2 distance(vec3 o) {
	return vec2(plane(o - vec3(0.0, 0.0, -4.0), vec3(0.0, 1.0, 0.0), 0.0) + wave(o.xz), material1);
}

vec3 normal(vec3 target) {
	vec2 eps = vec2(0.0001, 0.0);
	return normalize(vec3(distance(target + eps.xyy).x - distance(target - eps.xyy).x, distance(target + eps.yxy).x - distance(target - eps.yxy).x , distance(target + eps.yyx).x - distance(target - eps.yyx).x));
}

vec3 pack_normal(vec3 n) {
	return n * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
}

vec2 march(vec3 pos, vec3 dir) {
	float travel = epsilon * 2.0;
	for (float i = 0.0; travel < far && i < marchIterations; i++) {
		vec2 hit = distance(pos + dir * travel);
		float d = length(hit.x);
		if (d < epsilon || travel > far) {
			return vec2(travel, hit.y);
			break;
		}
		travel += d * 0.5;
	}
	return vec2(travel, material0);
}

void phong(inout vec3 color, vec3 pos, vec3 n, vec3 rayDir, vec3 refRayDir, vec3 lightColor, vec3 specColor) {
	vec3 lightDir = normalize(vec3(0.0, 1.0, -1.0));
	float intensity = dot(n, lightDir);
	intensity = max(0.3, intensity);

	color *= intensity * lightColor;
	float spec = pow(clamp(dot(refRayDir, lightDir), 0.0, 1.0), specularPower);
	color += specColor * pow(clamp(spec * intensity * 1.0, 0.0, 1.0), 4.0) * 4.0;
}

vec3 lighting(vec3 rayPos, vec3 rayDir) {
	vec3 totalColor = vec3(0.0, 0.0, 0.0);

	vec2 hit = march(rayPos, rayDir);
	float hitDist = hit.x;
	if (hitDist > far) {
		return mix(bottomSkyColor, topSkyColor, pow(clamp(rayDir.y, 0.0, 1.0), 0.75));
	}
	vec3 hitPos = rayPos + rayDir * hitDist;
	vec3 n = normal(hitPos);
	vec3 refRayDir = reflect(rayDir, n);

	vec3 color = mix(vec3(0.0, 0.15, 0.2), vec3(0.3, 0.8, 0.7), (pow(sin(clamp(hitPos.y + 5.0, 0.0, pie) * 0.2), 2.0)));
	phong(color, hitPos, n, rayDir, refRayDir, vec3(0.5, 0.7, 1.0), vec3(1.0, 1.0, 1.0));

	float fresnel = pow(1.0 - clamp(dot(n, -rayDir), 0.0, 1.0), 2.5) * 0.5;
	color += vec3(1.0, 1.0, 1.0) * fresnel;

	totalColor += color;
	totalColor = mix(totalColor, bottomSkyColor, hitDist / far);

	return totalColor;
}

in vec2 var_texCoord0;
out vec4 out_fragColor;

void main() {
	vec2 uv = var_texCoord0.xy;

	vec3 top = mix(eyePlane[0], eyePlane[1], uv.x);
	vec3 bottom = mix(eyePlane[2], eyePlane[3], uv.x);
	vec3 rayDir = normalize(mix(bottom, top, uv.y));

	vec3 color = lighting(eyePosition, rayDir);
	out_fragColor = vec4(color.x, color.y, color.z, 1);
}
