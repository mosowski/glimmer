uniform vec3 eyePosition;
uniform vec3 eyeDirection;
uniform vec3 eyePlane[4];
uniform vec3 times;

#define marchIterations 350.0
#define epsilon 0.003
#define far 1500.0

float cone(vec3 o, vec2 pt) {
	return length(o.xz - pt) - o.y;
}

void sum(inout vec4 a, vec4 b) {
	if (b.x < a.x) {
		a = b;
	}
}

vec4 distance(vec3 o) {
	// cones
	/*return d;*/

	float s = 0.3;

	vec4 d = vec4(cone(o, vec2(30.0*sin(times.x), 0.0)*s), vec3(1.0, 0.0, 0.0));
	sum(d, vec4(cone(o, vec2(-30.0, 0.0)*s), vec3(0.0, 0.0, 1.0)));
	sum(d, vec4(cone(o, vec2(0.0, 60.0*cos(times.x*0.5)*s)), vec3(0.0, 1.0, 0.0)));
	sum(d, vec4(cone(o, vec2(140.0, -120.0)*s), vec3(1.0, 1.0, 0.0)));
	sum(d, vec4(cone(o, vec2(-80.0, -100.0)*s), vec3(0.0, 1.0, 1.0)));
	sum(d, vec4(cone(o, vec2(50.0+10.0*sin(times.x), 30.0))*s, vec3(1.0, 1.0, 1.0)));
	return d;
}

vec3 normal(vec3 target) {
	vec2 eps = vec2(0.0001, 0.0);
	return normalize(vec3(distance(target + eps.xyy).x - distance(target - eps.xyy).x, distance(target + eps.yxy).x - distance(target - eps.yxy).x , distance(target + eps.yyx).x - distance(target - eps.yyx).x));
}

vec4 march(vec3 pos, vec3 dir) {
	float travel = epsilon * 2.0;
	vec3 color = vec3(0.0, 0.0, 0.0);
	for (float i = 0.0; travel < far && i < marchIterations; i++) {
		vec4 hit = distance(pos + dir * travel);
		color = hit.yzw;
		float d = length(hit.x);
		if (d < epsilon || travel > far) {
			return vec4(travel, color);
			break;
		}
		travel += d * 0.5;
	}
	return vec4(travel, color);
}

vec3 pack_normal(vec3 n) {
	return n * 0.5 + 0.5;
}

vec3 voronoi(vec3 rayPos, vec3 rayDir) {
	vec4 hit = march(rayPos, rayDir);
	vec3 color = hit.yzw;
	float hitDist = hit.x;
	vec3 hitPos = rayPos + rayDir * hitDist;
	vec3 n = normal(hitPos);
	/*return (color + pack_normal(n)) * 0.5;*/
	return color;
}

in vec2 var_texCoord0;

void main() {
	vec2 uv = var_texCoord0.xy;

	vec3 top = mix(vec3(-1.0, 1.0, 1.0), vec3(1.0, 1.0, 1.0), uv.x);
	vec3 bottom = mix(vec3(-1.0, 1.0, -1.0), vec3(1.0, 1.0, -1.0), uv.x);
	vec3 rayDir = normalize(mix(bottom, top, uv.y));

	vec3 color = voronoi(rayDir * 100.0, vec3(0.0, -1.0, 0.0));
	gl_FragColor = vec4(color.x, color.y, color.z, 1);
}
