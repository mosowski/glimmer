//###begin
// texture noiseTexture textures/noise-rgba.png repeat linear
//###end
uniform vec3 eyePosition;
uniform vec3 eyeDirection;
uniform vec3 eyePlane[4];
uniform vec3 times;
uniform sampler2D noiseTexture;

#define marchIterations 150.0
#define epsilon 0.0003
#define far 500.0
#define reflections 1.0
#define softShadowFactor 16.0
#define specularPower 64.0
#define bottomSkyColor vec3(0.64, 0.77, 0.8)
#define topSkyColor vec3(0.2, 0.66, 0.75)

#define material0 1024.0
#define material1 1.0
#define material2 2.0
#define material3 4.0
#define material4 8.0

#define pie 3.14159265
#define cloudSpeed 0.0

float noise(in vec3 o) {
	vec3 p = floor(o);
	vec3 f = fract(o);
	f = f*f*(3.0 - 2.0*f);
	vec2 uv = (p.xy+vec2(37.0,17.0)*p.z) + f.xy;
	vec2 rg = texture2D(noiseTexture, (uv+ 0.5)/256.0, -100.0 ).yx;
	return -1.0+2.0*mix(rg.x, rg.y, f.z );
}

float volume_noise_lod1(in vec3 p) {
	vec3 w = vec3(0.5,0.0,1.0)* times.x * cloudSpeed;
	vec3 q = p - w;
	float f;
    f  = 0.5*noise( q ); q = q*2.02 + w.xxx*5.0;
    f += 0.25*noise( q ); q = q*2.03 + w.xyz *4.0;
    f += 0.125*noise( q ); q = q*2.01 + w.yxy *3.0;
    f += 0.0625*noise( q ); q = q*2.02 + w.zyy*6.0;
    f += 0.03125*noise( q );
	return clamp( p.y - 10.0 + 5.75*f, 0.0, 1.0 );
}

float volume_noise_lod2( in vec3 p ) {
	vec3 w = vec3(0.5,0.0,1.0)* times.x * cloudSpeed;
	vec3 q = p - w;
	float f;
    f  = 0.5*noise( q ); q = q*2.02 + w.xxx*5.0;
    f += 0.25*noise( q ); q = q*2.03 + w.xyz *4.0;
    f += 0.125*noise( q ); q = q*2.01 + w.yxy *3.0;
    f += 0.0625*noise( q ); q = q*2.02 + w.zyy*6.0;
	return clamp( p.y - 10.0 + 5.75*f, 0.0, 1.0 );
}

float volume_noise_lod3( in vec3 p ) {
	vec3 w = vec3(0.5,0.0,1.0)* times.x * cloudSpeed;
	vec3 q = p - w;
	float f;
    f  = 0.5*noise( q ); q = q*2.02 + w.xxx*5.0;
    f += 0.25*noise( q ); q = q*2.03 + w.xyz *4.0;
    f += 0.125*noise( q ); q = q*2.01 + w.yxy *3.0;
	return clamp( p.y - 10.0 + 5.75*f, 0.0, 1.0 );
}

float volume_noise_lod4( in vec3 p ) {
	vec3 w = vec3(0.5,0.0,1.0)* times.x * cloudSpeed;
	vec3 q = p - w;
	float f;
    f  = 0.5*noise( q ); q = q*2.02 + w.xxx*4.0;
    f += 0.25*noise( q ); q = q*2.03 + w.xyz *4.0;
	return clamp( p.y - 10.0 + 5.75*f, 0.0, 1.0 );
}

float cloud_density(in vec3 o, in float i) {
	o *= vec3(0.25, 0.25, 0.25);
	float w = 30.0;
	if (i < 100.0*w) {
		return volume_noise_lod1(o);
	} else if (i < 2.0 * w) {
		return volume_noise_lod2(o);
	} else if (i < 3.0 * w) {
		return volume_noise_lod3(o);
	} else {
		return volume_noise_lod4(o);
	}
}

float plane(vec3 o, vec3 normal, float offset) {
	return dot(o, normal) - offset;
}

vec2 distance(vec3 o) {
	return vec2(plane(o, vec3(0.0, 1.0, 0.0), -1.0), material1);
}

vec3 normal(vec3 target) {
	vec2 eps = vec2(0.0001, 0.0);
	return normalize(vec3(distance(target + eps.xyy).x - distance(target - eps.xyy).x, distance(target + eps.yxy).x - distance(target - eps.yxy).x , distance(target + eps.yyx).x - distance(target - eps.yyx).x));
}

vec3 pack_normal(vec3 n) {
	return n * vec3(0.5, 0.5, 0.5) + vec3(0.5, 0.5, 0.5);
}

vec2 march(vec3 pos, vec3 dir) {
	float travel = epsilon * 5.0;
	for (float i = 0.0; travel < far && i < marchIterations; i++) {
		vec2 hit = distance(pos + dir * travel);
		float d = length(hit.x);
		if (d < epsilon || travel > far) {
			return vec2(travel, hit.y);
			break;
		}
		travel += d * 0.5;
	}
	return vec2(travel, material0);
}

vec3 march_clouds(vec3 pos, vec3 dir, vec3 solidColor) {
	vec3 lightDir = normalize(vec3(0.0, 1.0, -1.0));

	vec4 clouds = vec4(0.0, 0.0, 0.0, 0.0);

	float travel = 4.0;
	for (float i = 0.0; i < 120.0; i++) {
		vec3 p = pos + dir * travel;

		float density = cloud_density(p, i);
		if (density > 0.01) {
			float dif = clamp((density - cloud_density(p + 0.5 * lightDir, i)) / 0.5, 0.0, 1.0);

			vec3 lin = vec3(0.65, 0.68, 0.7) * 1.3 + 0.5 * vec3(0.7, 0.5, 0.3) * dif;        
			vec4 col = vec4( mix( 1.3 * vec3(1.0,0.92,0.72), vec3(0.60), density), density);
			col.xyz *= lin;
			col.xyz = mix(col.xyz, solidColor, 1.0 - exp(-0.0005 * travel * travel) );

			col.a *= 0.4;
			col.rgb *= col.a;
			
			clouds += col * (1.0 - clouds.a) *0.5;
		}

		if (clouds.a > 0.99) {
			break;
		}

		travel += max(0.01, 0.02 * travel);
	}
    solidColor = solidColor * (1.0 - clouds.w) + clouds.xyz;
	return solidColor;
}

void phong(inout vec3 color, vec3 pos, vec3 n, vec3 rayDir, vec3 refRayDir, vec3 lightColor, vec3 specColor) {
	vec3 lightDir = normalize(vec3(0.0, 1.0, -1.0));
	float intensity = dot(n, lightDir);
	intensity = max(0.3, intensity);

	color *= intensity * lightColor;
	float spec = pow(clamp(dot(refRayDir, lightDir), 0.0, 1.0), specularPower);
	color += specColor * pow(clamp(spec * intensity * 1.0, 0.0, 1.0), 4.0) * 4.0;
}

vec3 lighting(vec3 rayPos, vec3 rayDir) {
	vec3 totalColor = vec3(0.0, 0.0, 0.0);

	vec2 hit = march(rayPos, rayDir);
	float hitDist = hit.x;
	if (hitDist > far) {
		return mix(bottomSkyColor, topSkyColor, pow(clamp(rayDir.y, 0.0, 1.0), 0.75));
	}
	vec3 hitPos = rayPos + rayDir * hitDist;
	vec3 n = normal(hitPos);
	vec3 refRayDir = reflect(rayDir, n);

	vec3 color = mix(vec3(0.0, 0.15, 0.2), vec3(0.3, 0.8, 0.7), (pow(sin(clamp(hitPos.y + 5.0, 0.0, pie) * 0.2), 2.0)));
	phong(color, hitPos, n, rayDir, refRayDir, vec3(0.5, 0.7, 1.0), vec3(1.0, 1.0, 1.0));

	float fresnel = pow(1.0 - clamp(dot(n, -rayDir), 0.0, 1.0), 2.5) * 0.5;
	color += vec3(1.0, 1.0, 1.0) * fresnel;

	totalColor += color;
	totalColor = mix(totalColor, bottomSkyColor, hitDist / far);

	return totalColor;
}

in vec2 var_texCoord0;
out vec4 out_fragColor;

void main() {
	vec2 uv = var_texCoord0.xy;

	vec3 top = mix(eyePlane[0], eyePlane[1], uv.x);
	vec3 bottom = mix(eyePlane[2], eyePlane[3], uv.x);
	vec3 rayDir = normalize(mix(bottom, top, uv.y));

	vec3 color = lighting(eyePosition, rayDir);
	color = march_clouds(eyePosition, rayDir, color);
	out_fragColor = vec4(color.x, color.y, color.z, 1);
}

