out vec2 var_texCoord0;

void main() {	
	var_texCoord0 = gl_MultiTexCoord0;
	gl_Position = gl_ProjectionMatrix * gl_ModelViewMatrix * gl_Vertex;
}
